def vowel_swapper(string):
    # ==============
    # Your code here
    newStr = string.replace('a', '4')
    newStr = newStr.replace('A', '4')
    newStr = newStr.replace('e', '3')
    newStr = newStr.replace('E', '3')
    newStr = newStr.replace('i', '!')
    newStr = newStr.replace('I', '!')
    newStr = newStr.replace('o', 'ooo')
    newStr = newStr.replace('O', '000')
    newStr = newStr.replace('u', '|_|')
    newStr = newStr.replace('U', '|_|')
    return newStr
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
